<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: fail</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>Oracle...</title>
			<link>http://oxygene.sk/2010/06/oracle/</link>
			<comments>http://oxygene.sk/2010/06/oracle/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;Working with Oracle is always an adventure. The error messages are usually not very helpful, so you have to guess a lot. What I&#39;ve seen today is an extreme though. Oracle allows you to create a table with a column named &quot;TIMESTAMP&quot; if you quote it:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE TABLE &quot;SOME_TABLE&quot; (
    ...
    &quot;TIMESTAMP&quot; TIMESTAMP WITH TIME ZONE
);
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Oracle is rather picky on identifier names, but since it accepted &quot;TIMESTAMP&quot;, I was assuming everything is fine. Later I needed to create a trigger for this table and that&#39;s where the fun starts.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE TRIGGER &quot;SOME_TABLE_TR&quot; 
BEFORE INSERT ON &quot;SOME_TABLE&quot;
FOR EACH ROW
BEGIN
    ...
END;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This was failing for some reason though. The only thing I got was this &quot;nice&quot; error message, pointing to the table name in the &lt;code&gt;CREATE TRIGGER&lt;/code&gt; statement:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;ORA-06552: PL/SQL: Compilation unit analysis terminated
ORA-06553: PLS-320: the declaration of the type of this expression is incomplete or malformed
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;What type? Do I have a typo somewhere? Did the table somehow get corrupted? You can&#39;t imaging how long did it take for me to figure out that it doesn&#39;t like the column name, which was not mentioned anywhere in the PL/SQL block. I would have no problem if it told me that I can&#39;t use the name. There are too many restrictions on identifiers anyway. What I don&#39;t understand is why does it allow me to create something that&#39;s going to break other core functionality.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=495</guid>
			
			<pubDate>Wed, 16 Jun 2010 16:37:40 -0000</pubDate>
		</item>
		
		<item>
			<title>Fun with timestamps</title>
			<link>http://oxygene.sk/2010/03/fun-with-timestamps/</link>
			<comments>http://oxygene.sk/2010/03/fun-with-timestamps/#disqus_thread</comments>
			
			<category>programming</category>
			
			<description>&lt;p&gt;Some programming languages really encourage using &lt;a href=&quot;http://en.wikipedia.org/wiki/Unix_time&quot;&gt;UNIX timestamps&lt;/a&gt; for working with dates. PHP is a good example of such a language. Functions like &lt;a href=&quot;http://php.net/manual/en/function.date.php&quot;&gt;&lt;code&gt;date&lt;/code&gt;&lt;/a&gt;, &lt;a href=&quot;http://php.net/manual/en/function.strtotime.php&quot;&gt;&lt;code&gt;strtotime&lt;/code&gt;&lt;/a&gt;, &lt;a href=&quot;http://php.net/manual/en/function.strftime.php&quot;&gt;&lt;code&gt;strftime&lt;/code&gt;&lt;/a&gt; are used all the time. Most people don&#39;t realize that timestamps in general can&#39;t really be used for calculations though. The problem is that most countries use &lt;a href=&quot;http://en.wikipedia.org/wiki/Daylight_saving_time&quot;&gt;daylight saving time&lt;/a&gt;, which means that two times a year the local timezone changes. This nicely breaks the assumption that every day has 24 hours. It doesn&#39;t. Sometimes it has 23 or 25 hours.&lt;/p&gt;

&lt;p&gt;This &lt;a href=&quot;http://stackoverflow.com/questions/2361222/php-date-function-finding-the-previous-week/2361335#2361335&quot;&gt;StackOverflow answer&lt;/a&gt; is a nice example of the problem:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;$mondayStr = &quot;last monday&quot;;
if (date(&#39;N&#39;) !== &#39;1&#39;) {  // it&#39;s not Monday today
    $mondayStr .= &quot; last week&quot;;
}

$monday = strtotime($mondayStr);
echo date(&#39;r&#39;, $monday);    // Mon, 22 Feb 2010 00:00:00 +1000

$sunday = $monday + 86400 * 7 - 1;
echo date(&#39;r&#39;, $sunday);    // Sun, 28 Feb 2010 23:59:59 +1000
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;The code seems logical. Get the timestamp of the last Monday, add 60 * 60 * 24 * 7 - 1 seconds and you have the end of Sunday. Works fine most of the time. Although, if the &lt;code&gt;$monday&lt;/code&gt; happens to be &lt;em&gt;Mon, 22 Mar 2010 00:00:00&lt;/em&gt;, the date that was supposed to be &lt;em&gt;Sun, 28 Mar 2010  23:59:59&lt;/em&gt; will actually be &lt;em&gt;Mon, 29 Mar 2010 00:59:59&lt;/em&gt;. Why? Because 28 March 2010 has only 23 hours.&lt;/p&gt;

&lt;p&gt;Never use timestamps to do calendar calculations. It&#39;s hard to get it right. If you really have to, at least use GMT timestamps.&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=342</guid>
			
			<pubDate>Tue, 02 Mar 2010 16:32:56 -0000</pubDate>
		</item>
		
		<item>
			<title>Mercurial, part 1</title>
			<link>http://oxygene.sk/2010/01/mercurial-part-1/</link>
			<comments>http://oxygene.sk/2010/01/mercurial-part-1/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I&#39;ve been using Mercurial at work for two months now and the expectations I had about it didn&#39;t change to anything better. I guess it looks cool for people who are used to SVN, or even CVS, and are not familiar with other DVCS. I must say that as a Bazaar user, I miss a lot of things. There are some that can be worked around, for something you just have to be extra careful and for something you are out of luck. I really don&#39;t get how people use Mercurial for managing large projects.&lt;/p&gt;

&lt;h3&gt;Cheap Branches&lt;/h3&gt;

&lt;p&gt;One of the main reason people use DVCS is cheap branching. The most surprising thing for me was that Mercurial doesn&#39;t offer many choices. It doesn&#39;t have the concept of branches, like Bazaar or Git. All you have is an immutable repository that can contain multiple head revisions. By default, you are expected to clone the whole repository. As &lt;a href=&quot;/2009/10/working-with-branches-in-bazaar/&quot;&gt;I mentioned earlier&lt;/a&gt;, I tend to work with many feature branches, so I prefer to have just one working tree.&lt;/p&gt;

&lt;p&gt;One way to partially solve this problem is the &lt;a href=&quot;http://mercurial.selenic.com/wiki/BookmarksExtension&quot;&gt;bookmarks extension&lt;/a&gt;. It basically allows you to have dynamic tags that move to new revisions as you commit. The initial configuration looks like this:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;[extensions]
bookmarks =
[bookmarks]
track.current = True
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;The next step is to go to your clone and set the main/trunk/master/etc. bookmark:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;hg bookmark main
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This is necessary, so that you easily know which revisions represents the project&#39;s &quot;mainline&quot;. With the configuration I mentioned, the extension will track your current bookmark and update only that one. To set the current bookmark, you can use update:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;hg up -r main
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;It&#39;s important to always have the current bookmark set to &lt;em&gt;main&lt;/em&gt; whenever you are fetching changes from the mainline. Otherwise you will have to manually fix the bookmark&#39;s revision.&lt;/p&gt;

&lt;p&gt;Creating new &quot;feature branches&quot; is easy then:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;hg bookmark -r main my-feature
hg up -r my-feature
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;One important thing to remember is that after you use this, the repository will contain multiple heads. This means you can&#39;t use plain &lt;code&gt;hg push&lt;/code&gt; to push changes to remote repositories. You always have to specify the revision, for example:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;hg push -r main https://hg.example.com/projects/trunk
&lt;/code&gt;&lt;/pre&gt;

&lt;h3&gt;Merging&lt;/h3&gt;

&lt;p&gt;When it comes to merging, Mercurial doesn&#39;t make your life easier at all. The default assumption is that visual merging is the preferred way to go. Maybe I&#39;m just too stupid, but I don&#39;t get visual merge tools. I just don&#39;t know how to use them. I prefer merging changes manually in my text editor, after I see the changes for the whole project. Mercurial would start up the merge tool for each file linearly, which means you don&#39;t have a global picture of impact on the project, when you are supposed to resolve a merge. I just can&#39;t work that way.&lt;/p&gt;

&lt;p&gt;Fortunately, Mercurial does have an internal three-way merge algorithm, that can leave conflict markers in the merged files and let me do my job. You can configure it this way:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;[ui]
merge = internal:merge
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This actually uses the merge code from Bazaar, so I expected I&#39;ll be done quickly. There is one problem though. Instead of using patience diff, like Bazaar, it uses its own diff algorithm. The result is that in Bazaar the algorithm works just fine, in Mercurial it produces horrible results. That&#39;s actually pretty funny, considering the fact that I used ideas from Mercurial&#39;s diff code to implement the C version of patience diff in Bazaar.&lt;/p&gt;

&lt;p&gt;Next step, the &lt;a href=&quot;http://mercurial.selenic.com/wiki/MergeProgram&quot;&gt;Mercurial wiki&lt;/a&gt; suggests &lt;code&gt;diff3&lt;/code&gt; as a possible non-interactive merge tool. There are no examples how to configure it, but something like this technically works:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;[ui]
merge = diff3
[merge-tools]
diff3.args = -m $local $base $other &amp;gt; $output
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;In my experience, this produces even worse results than &lt;code&gt;internal:merge&lt;/code&gt;, so there is no reason to use it.&lt;/p&gt;

&lt;p&gt;The last idea was to use the &lt;code&gt;merge&lt;/code&gt; program from &lt;a href=&quot;http://www.gnu.org/software/rcs/&quot;&gt;RCS&lt;/a&gt; (yes, that&#39;s right, using an ancient VCS to fix issues in a &quot;modern&quot; VCS). I must say that installing RCS in 2010 feels weird, but as long as I can get my job done... Once it was installed, I used this configuration:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;[ui]
merge = merge
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;I was surprised that the results were acceptably good. Not perfect, but I knew I can&#39;t expect more from a three-way merge. Ideally I&#39;d like Bazaar&#39;s merge algorithm with patience diff, but I probably won&#39;t find time to port it, so &lt;code&gt;merge&lt;/code&gt; will stay as my default merge tool for some time.&lt;/p&gt;

&lt;p&gt;That&#39;s only the first part of the problem though. Now you have files with conflict markers and you need to resolve them. Based on my experience with other version control systems, I assumed conflicts would be listed in &lt;code&gt;hg status&lt;/code&gt;, but I was wrong. They are nicely hidden here:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;hg resolve -l | grep &#39;^U&#39;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;After manually resolving the conflicts, you can use the following command to mark the files as resolved:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;hg resolve -m path/to/file
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;&lt;em&gt;To be continued...&lt;/em&gt;&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=283</guid>
			
			<pubDate>Sat, 23 Jan 2010 05:00:38 -0000</pubDate>
		</item>
		
		<item>
			<title>Technical book authors</title>
			<link>http://oxygene.sk/2010/01/technical-book-authors/</link>
			<comments>http://oxygene.sk/2010/01/technical-book-authors/#disqus_thread</comments>
			
			<description>&lt;p&gt;To be honest, I don&#39;t read technical books much. I prefer reading the official documentation for a product I need to work with, or use some other ways to get information about it. I always assumed people who write such books are experts though. There are many book authors on Stack Overflow, but today I encountered a particularly interesting situation with one of them.&lt;/p&gt;

&lt;p&gt;Somebody asked a &lt;a href=&quot;http://stackoverflow.com/questions/1993050/how-can-i-set-the-output-file-for-perls-filefetch&quot;&gt;question about a Perl module&lt;/a&gt; a few days ago and based on reading the source code of the module, I gave them one possible way to solve the problem. Today, &lt;a href=&quot;http://stackoverflow.com/users/8817/brian-d-foy&quot;&gt;an author of multiple Perl books&lt;/a&gt; came in and very confidently claimed that my solution will never work. I explained him why he is wrong, but it didn&#39;t help. The problem was that he failed to read the source code correctly and made incorrect assumptions based on that. I pointed him to specific parts of the code, still nothing. At that point I wanted to gave up, but later he came up with a broken example, which didn&#39;t work for different reasons, that should show me why I&#39;m wrong. Soon after that he realized what is wrong about the example, changed his answer and deleted all his old comments, as if they never happened.&lt;/p&gt;

&lt;p&gt;The problem I see is not this specific case (I&#39;ve seen far more of them, either on Stack Overflow or some IRC channels), but the fact that even average programmers write popular programming books. And people learn from these books. I guess the saying about teachers who failed to apply their knowledge in practice, which I can&#39;t remember right now, applies even for book authors. What a wonderful world.&lt;/p&gt;

&lt;p&gt;&lt;em&gt;(I just needed to get this out of myself.)&lt;/em&gt;&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=260</guid>
			
			<pubDate>Wed, 06 Jan 2010 09:56:27 -0000</pubDate>
		</item>
		
		<item>
			<title>Disabling sound in GDM</title>
			<link>http://oxygene.sk/2009/11/disabling-sound-in-gdm/</link>
			<comments>http://oxygene.sk/2009/11/disabling-sound-in-gdm/#disqus_thread</comments>
			
			<category>tools</category>
			
			<description>&lt;p&gt;I often have to turn on my laptop in a situation when I need it to be quiet. It&#39;s easy to disable the login sound in GNOME, but in the new Ubuntu release it became quite hard to disable the GDM startup sound. Previously it was possible to simply use &lt;code&gt;gdmsetup&lt;/code&gt; to change the sound, themes, etc. However, in recent versions of GDM (like the one included in Ubuntu 9.10), the window was reduced to a question whether it should log me in automatically or ask for the password. The old configuration file &lt;code&gt;gdm.conf&lt;/code&gt; is also gone, replaced with GConf-based configuration. The &lt;a href=&quot;http://library.gnome.org/admin/gdm/stable/configuration.html.en&quot;&gt;GDM documentation&lt;/a&gt; says, as an example, that sound can be disabled by changing the &lt;code&gt;/apps/gdm/simple-greeter/settings-manager-plugins/sound/active&lt;/code&gt; GConf key, so I tried to set it to &lt;code&gt;false&lt;/code&gt;, but that didn&#39;t help. I&#39;ve managed to fix it eventually, thanks to a &lt;a href=&quot;https://bugs.launchpad.net/ubuntu/+source/gdm/+bug/437429&quot;&gt;Ubuntu bug&lt;/a&gt;, where somebody mentions the &lt;code&gt;/desktop/gnome/sound/event_sounds&lt;/code&gt; key.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;sudo -u gdm gconftool-2 --set /desktop/gnome/sound/event_sounds --type bool false
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;I&#39;m still not sure &lt;em&gt;why&lt;/em&gt; does it work and why does this key affect GDM, but as long as it&#39;s quiet...&lt;/p&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=199</guid>
			
			<pubDate>Sun, 15 Nov 2009 16:31:26 -0000</pubDate>
		</item>
		
	</channel>
</rss>

