<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
	<channel>
		<title>Lukáš Lalinský - Tag: plpgsql</title>
		<description>Random notes and stuff</description>
		<link>http://oxygene.sk</link>
		<language>en</language>
		
		<item>
			<title>UUID generator in PL/pgSQL</title>
			<link>http://oxygene.sk/2009/10/uuid-generator-in-plpgsql/</link>
			<comments>http://oxygene.sk/2009/10/uuid-generator-in-plpgsql/#disqus_thread</comments>
			
			<category>programming</category>
			
			<description>&lt;p&gt;While writing an &lt;a href=&quot;http://bugs.musicbrainz.org/browser/mb_server/branches/RELEASE_20090524-BRANCH/admin/sql/updates/20090402-2.sql&quot;&gt;SQL script&lt;/a&gt; to upgrade the MusicBrainz database for the last release, I needed a way to generate new &lt;a href=&quot;http://en.wikipedia.org/wiki/Universally_Unique_Identifier&quot;&gt;UUIDs&lt;/a&gt; from SQL. PostgreSQL has a native UUID data type and a contrib module for generating UUIDs since version 8.3, but this wouldn&#39;t help me, because I needed it to work with at least version 8.1. I had this idea to write PL/pgSQL functions to generate UUIDs, so I skimmer over the &lt;a href=&quot;http://www.ietf.org/rfc/rfc4122.txt&quot;&gt;RFC 4122&lt;/a&gt; that documents them and found out that it isn&#39;t actually that hard.&lt;/p&gt;

&lt;p&gt;MusicBrainz uses random-based UUIDs (version 4) for all it&#39;s new IDs, so the first idea was to implement the same. I know I can&#39;t use this code in the end, because I need a good pseudo-random number generator, but I couldn&#39;t resist to write it anyway. Messing with bits in high-level languages is always fun :) Here is the result (&lt;em&gt;because of the use of the &lt;code&gt;random()&lt;/code&gt; function, don&#39;t use the code for anything serious&lt;/em&gt;):&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE FUNCTION generate_uuid_v4() RETURNS uuid
    AS $$
DECLARE
    value VARCHAR(36);
BEGIN
    value =          lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad((to_hex((ceil(random() * 255)::int &amp;amp; 15) | 64)), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad((to_hex((ceil(random() * 255)::int &amp;amp; 63) | 128)), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    value = value || lpad(to_hex(ceil(random() * 255)::int), 2, &#39;0&#39;);
    RETURN value::uuid;
END;
$$ LANGUAGE &#39;plpgsql&#39;;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;It turned out that we need deterministic IDs to be generated from the script, so V4 was out of question. That was good, because we would need a better PNRG for the final version.&lt;/p&gt;

&lt;p&gt;The next idea was to create the URL on which the new rows will be server and generate name-based UUIDs using the URL namespace. The idea is to concatenate a namespace and a name, calculate a cryptographic hash of the result, and use it&#39;s bits to generate the UUID.  There are two options for hashing, either MD5 (version 3) or SHA-1 (version 5). SHA-1 is preferred by the RFC, but PostgreSQL only has a built-in function for MD5, so the decision for us was easy. The code doesn&#39;t depend any random numbers, so it&#39;s good enough to use in production.&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;CREATE OR REPLACE FUNCTION from_hex(t text) RETURNS integer
    AS $$
DECLARE
    r RECORD;
BEGIN
    FOR r IN EXECUTE &#39;SELECT x&#39;&#39;&#39;||t||&#39;&#39;&#39;::integer AS hex&#39; LOOP
        RETURN r.hex;
    END LOOP;
END
$$ LANGUAGE plpgsql IMMUTABLE STRICT;

CREATE OR REPLACE FUNCTION generate_uuid_v3(namespace varchar, name varchar) RETURNS uuid
    AS $$
DECLARE
    value varchar(36);
    bytes varchar;
BEGIN
    bytes = md5(decode(namespace, &#39;hex&#39;) || decode(name, &#39;escape&#39;));
    value = substr(bytes, 1+0, 8);
    value = value || &#39;-&#39;;
    value = value || substr(bytes, 1+2*4, 4);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex((from_hex(substr(bytes, 1+2*6, 2)) &amp;amp; 15) | 48), 2, &#39;0&#39;);
    value = value || substr(bytes, 1+2*7, 2);
    value = value || &#39;-&#39;;
    value = value || lpad(to_hex((from_hex(substr(bytes, 1+2*8, 2)) &amp;amp; 63) | 128), 2, &#39;0&#39;);
    value = value || substr(bytes, 1+2*9, 2);
    value = value || &#39;-&#39;;
    value = value || substr(bytes, 1+2*10, 12);
    return value::uuid;
END;
$$ LANGUAGE &#39;plpgsql&#39; IMMUTABLE STRICT;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This code should be easy enough to modify to generate UUIDv5, if you have a way to calculate SHA-1 hashes. To use the function, you need to pass it a namespace and a name. The namespace itself is a UUID, it can be anything, but there are a few well-known options:&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;&lt;strong&gt;URL&lt;/strong&gt;&lt;br /&gt;&lt;code&gt;&#39;6ba7b8119dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;DNS&lt;/strong&gt; (fully-qualified domain name)&lt;br /&gt;&lt;code&gt;&#39;6ba7b8109dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;ISO OID&lt;/strong&gt;&lt;br /&gt;&lt;code&gt;&#39;6ba7b8129dad11d180b400c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;&lt;strong&gt;X.500 DN&lt;/strong&gt; (in DER or a text output format)&lt;br /&gt;&lt;code&gt;&#39;6ba7b814-9dad-11d1-80b4-00c04fd430c8&#39;&lt;/code&gt;&lt;/li&gt;
&lt;/ul&gt;


&lt;p&gt;The URL one is probably the most useful. So, to generate UUIDv3 for &lt;code&gt;http://www.example.com/foo/1&lt;/code&gt;, you can use the following:&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;SELECT generate_uuid_v3(&#39;6ba7b8119dad11d180b400c04fd430c8&#39;, &#39;http://www.example.com/foo/1&#39;);
&lt;/code&gt;&lt;/pre&gt;
</description>
			
			<guid isPermaLink="false">http://oxygene.sk/lukas/?p=38</guid>
			
			<pubDate>Wed, 21 Oct 2009 05:39:18 -0000</pubDate>
		</item>
		
	</channel>
</rss>

